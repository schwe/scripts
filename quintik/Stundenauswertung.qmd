---
title: "Stunden Florian 2024"
---

```{r, echo = FALSE, output = FALSE}
knitr::opts_chunk$set(echo = FALSE, output = FALSE)
CACHE_DOWNLOAD = TRUE
```

```{r, echo = FALSE, output = FALSE, cache = CACHE_DOWNLOAD}
library("readODS")
library("openxlsx")
library("snakecase")

read_data <- function() {
  url <- "https://docs.google.com/spreadsheets/d/1f1JN6EyoDXHueJVWi5hTa6b0r0DOABxFZplrJSKCDGw/export?format=ods"
  destfile <- tempfile()
  on.exit(unlink(destfile))
  auxi::download_file(url, destfile)
  if (!file.exists(destfile)) {
  stop("download failed")
  }
  sheets <- readODS::list_ods_sheets(destfile)
  d <- readODS::read_ods(destfile, sheet = "quintik")
  return(d)
}

refine <- function(x) {
  ncol <- 10
  x <- x[, seq_len(ncol)]
  empty_rows <- apply(x, 1, function(row) sum(is.na(row)))
  i <- which(empty_rows >= ncol)
  if (length(i) > 0) {
    writeLines(sprintf("remove %i empty rows", length(i)))
    x <- x[-i, ]
  }
  colnames(x) <- to_snake_case(colnames(x))
  x  
}
```


```{r, echo = FALSE, output = FALSE, cache = CACHE_DOWNLOAD}
raw <- read_data()
d <- refine(raw)
d$datum <- as.Date(substr(d$datum, 5, 999), format = "%d.%m.%y")
stunden <- aggregate(summe_in_h ~ datum, data = d, FUN = function(x) sum(x, na.rm = TRUE))


d24 <- d[as.Date("2024-01-01") <= d$datum & d$datum < as.Date("2025-01-01"), ]
stunden24 <- aggregate(summe_in_h ~ datum, data = d24, FUN = function(x) sum(x, na.rm = TRUE))
```

## Übersicht
```{r, output = TRUE}
oview <- aggregate(summe_in_h ~ kunde, data = d, FUN = function(x) sum(x, na.rm = TRUE))
# oview <- rbind(oview, data.frame(kunde = "**Summe**", summe_in_h = sum(oview$summe_in_h)))
oview$summe_in_h <- round(oview$summe_in_h, 2)
knitr::kable(oview)
```

## Bank Austria
```{r}
ba <- d[d$kunde == "Bank Austria", ]
fire_1 <- ba[which(ba$projekt == "Fire-1"), ]
fire_2 <- ba[which(ba$projekt != "Fire-1"), ]
```

### Fire - 1
```{r, output = TRUE}
fire1_sum <- aggregate(summe_in_h ~ projekt, fire_1, FUN = sum)
fire1_sum$summe_in_h <- round(fire1_sum$summe_in_h, 2)
colnames(fire1_sum)[1:2] <- c("task", "stunden")
knitr::kable(fire1_sum)
```

### Fire - 2
```{r, output = TRUE}
fire2_sum <- aggregate(summe_in_h ~ projekt, fire_2, FUN = sum)
fire2_sum$summe_in_h <- round(fire2_sum$summe_in_h, 2)
colnames(fire2_sum)[1:2] <- c("task", "stunden")
fire2_sum <- rbind(fire2_sum, data.frame(task = "**Summe**", stunden = sum(fire2_sum$stunden)))
knitr::kable(fire2_sum)
```